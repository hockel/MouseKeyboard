# USB模拟鼠标键盘


![](./arduinoC/_images/featured.png)

---------------------------------------------------------

## 目录

* [相关链接](#相关链接)
* [描述](#描述)
* [积木列表](#积木列表)
* [示例程序](#示例程序)
* [许可证](#许可证)
* [支持列表](#支持列表)
* [更新记录](#更新记录)

## 相关链接
* 本项目加载链接:``` https://gitee.com/hockel/MouseKeyboard```
* 4*4 矩阵键盘库：```https://gitee.com/hockel/keypad```
* AFMotor电机扩展板库：```https://gitee.com/hockel/AFMotor```
* 用户库教程链接: ```https://mindplus.dfrobot.com.cn/extensions-user```

## 描述
本扩展仅支持leonardo开发板，原因是它的核心是mega32u4，芯片带了硬件的USB从机外设。而uno的核心mega328，硬件上并不带usb的外设（当然也有牛人用汇编操作IO模拟实现了低速USB通信：v-usb。个人能力有限就不做相关开发了）

本扩展模拟了usb鼠标和键盘：

- 支持了鼠标点击，移动等操作
- 支持键盘输入等操作。

## 积木列表

![](./arduinoC/_images/blocks.png)



## 示例程序

### 电路连接

![](./arduinoC/_images/temp.png)

### 示例一：模拟鼠标移动点击操作

![](./arduinoC/_images/example.png)

### 示例二：PPT翻页笔样例程序

![](./arduinoC/_images/example1.png)

### 示例三：小恐龙游戏

打开Google浏览器输入：chrome://dino 即可开玩啦。（这里只用了一个光线传感器，所以对翼龙那关没有用，需要再加传感器）

### 硬件连接

![](./arduinoC/_images/temp1.png)



![](./arduinoC/_images/example2.png)

**详细样例教程欢迎访问个人博客：www.hockel.club**



## 许可证

MIT

## 支持列表

主板型号                | 实时模式    | ArduinoC   | MicroPython    | 备注
------------------ | :----------: | :----------: | :---------: | -----
leonardo        |             |       √       |             | 


## 更新日志
* V0.0.1  基础功能完成

  