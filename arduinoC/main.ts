
enum SELECT {
   //% block="打开"
   begin,
   //% block="关闭"
   end,    

}

enum OP {
    //% block="点击"
    click,
    //% block="按下"
    press,
    //% block="释放"
    release
}

enum MOUSEOP {
    //% block="左键"
    MOUSE_LEFT,
    //% block="中键"
    MOUSE_MIDDLE,
    //% block="右键"
    MOUSE_RIGHT
}
enum KOP{
    //% block="按下"
    press,
    //% block="释放"
    release    
}
enum KINPUT{
     //% block="左Ctrl键"
     KEY_LEFT_CTRL,
     //% block="左Shift键"
     KEY_LEFT_SHIFT,
     //% block="左ALT键"
     KEY_LEFT_ALT,
     //% block="左GUI键"
     KEY_LEFT_GUI,    
     //% block="右Ctrl键"
     KEY_RIGHT_CTRL, 
     //% block="右Shift键"
     KEY_RIGHT_SHIFT, 
     //% block="右ALT键"
     KEY_RIGHT_ALT,
     //% block="右GUI键"
     KEY_RIGHT_GUI,
     //% block="方向上键"
     KEY_UP_ARROW, 
     //% block="方向下键"
     KEY_DOWN_ARROW, 
     //% block="方向左键"
     KEY_LEFT_ARROW, 
     //% block="方向右键"
     KEY_RIGHT_ARROW, 
     //% block="退格键"
     KEY_BACKSPACE, 
     //% block="TAB键"
     KEY_TAB, 
     //% block="ENTER键"
     KEY_RETURN, 
     //% block="ESC键"
     KEY_ESC, 
     //% block="Delete键"
     KEY_DELETE, 
     //% block="PageUP键"
     KEY_PAGE_UP, 
     //% block="PageDown键"
     KEY_PAGE_DOWN, 
     //% block="Home键"
     KEY_HOME,  
    //% block="End键"
     KEY_END, 
     //% block="Capslock键"
     KEY_CAPS_LOCK, 
     //% block="F1键"
     KEY_F1, 
     //% block="F2键"
     KEY_F2, 
     //% block="F3键"
     KEY_F3, 
     //% block="F4键"
     KEY_F4, 
     //% block="F5键"
     KEY_F5, 
     //% block="F6键"
     KEY_F6, 
     //% block="F7键"
     KEY_F7, 
     //% block="F8键"
     KEY_F8, 
     //% block="F9键"
     KEY_F9, 
     //% block="F10键"
     KEY_F10,  
     //% block="F11键"
     KEY_F11, 
     //% block="F12键"
     KEY_F12
}
enum PRINT{
    //% block="字符串"
    print, 
    //% block="字符串(自动换行)"
    println
}


//% color="#00BFFF" iconWidth=50 iconHeight=40
namespace KeyboardMouse {
    //% block="初始化USB模拟鼠标[m]" blockType="command"
    //% m.shadow="dropdown" m.options="SELECT" m.defl="SELECT.打开"
    export function MouseInit(parameter: any, block: any) {
        let M1 = parameter.m.code;

        Generator.addInclude("MOUSEINCLUDE", "#include <Mouse.h>");
        Generator.addSetup(`MOUSE${M1}`, `Mouse.${M1}();`);
    }

    //% block="[Option]鼠标 [MOption]" blockType="command"
    //% Option.shadow="dropdown" Option.options="OP" Option.defl="OP.点击"
    //% MOption.shadow="dropdown" MOption.options="MOUSEOP" MOption.defl="MOUSEOP.左键"
   
    export function MouseOption(parameter: any, block: any) {
        let op = parameter.Option.code;
        let mop = parameter.MOption.code;

        Generator.addCode(`Mouse.${op}(${mop});`);
    }
    //% block="移动鼠标 X偏移量[XPOS] Y偏移量[YPOS] 滚轮偏移量[GPOS]" blockType="command"
    //% XPOS.shadow="number"  XPOS.defl="0"
    //% YPOS.shadow="number"  YPOS.defl="0"
    //% GPOS.shadow="number"  GPOS.defl="0"
   
    export function MouseMove(parameter: any, block: any) {
        let xpos = parameter.XPOS.code;
        let ypos = parameter.YPOS.code;
        let gpos = parameter.GPOS.code;
        Generator.addCode(`Mouse.move(${xpos}, ${ypos}, ${gpos});`);
    }

    //% block="鼠标 [MOption]是否被按下?" blockType="boolean"
    //% MOption.shadow="dropdown" MOption.options="MOUSEOP" MOption.defl="MOUSEOP.左键"
   
    export function MouseIsPressed(parameter: any, block: any) {
        let mop = parameter.MOption.code;

        Generator.addCode(`Mouse.isPressed(${mop})`);
    }
    //% block="初始化USB模拟键盘[m]" blockType="command"
    //% m.shadow="dropdown" m.options="SELECT" m.defl="SELECT.打开"
    export function KeyboardInit(parameter: any, block: any) {
        let M1 = parameter.m.code;

        Generator.addInclude("KEYBOARDINCLUDE", "#include <Keyboard.h>");
        Generator.addSetup(`KEYBOARD${M1}`, `Keyboard.${M1}();`);
    }

    //% block="[Option]键盘上 [KEY]" blockType="command"
    //% Option.shadow="dropdown" Option.options="KOP" Option.defl="KOP.按下"
    //% KEY.shadow="string"  KEY.defl="A"
   
    export function KeyboardOption(parameter: any, block: any) {
        let op = parameter.Option.code;
        let key = parameter.KEY.code;

        Generator.addCode(`Keyboard.${op}(${key});`);
    }
    //% block="[Option]键盘上 [KEY]" blockType="command"
    //% Option.shadow="dropdown" Option.options="KOP" Option.defl="KOP.按下"
    //% KEY.shadow="dropdown"  KEY.options="KINPUT" KEY.defl="KINPUT.左Ctrl键"
   
    export function KeyboardInput(parameter: any, block: any) {
        let op = parameter.Option.code;
        let key = parameter.KEY.code;

        Generator.addCode(`Keyboard.${op}(${key});`);
    }
    //% block="释放键盘上所有键" blockType="command"

    export function KeyboardReleaseALL(parameter: any, block: any) {

        Generator.addCode(`Keyboard.releaseAll();`);
    }
    //% block="键盘上发送字符 [VALUE]" blockType="command"
    //% VALUE.shadow="string"   VALUE.defl="A"
   
    export function KeyboardSendChar(parameter: any, block: any) {
        let value = parameter.VALUE.code;

        Generator.addCode(`Keyboard.write(char(${value}));`);
    }
    //% block="键盘上发送[msg] [VALUE]" blockType="command"
    //% msg.shadow="dropdown"   msg.options="PRINT" msg.defl="PRINT.字符串"
    //% VALUE.shadow="string"   VALUE.defl="ERISED"
   
    export function KeyboardSendString(parameter: any, block: any) {
        let message = parameter.msg.code;
        let value = parameter.VALUE.code;

        Generator.addCode(`Keyboard.${message}(${value});`);
    }
}
